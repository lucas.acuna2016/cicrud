CREATE TABLE estados(
	id_estado tinyint(1) unsigned not null primary key,
	nombre varchar(50)
)ENGINE=innoDB;

CREATE TABLE tareas(
	id_tarea tinyint(1) unsigned not null auto_increment primary key,
	titulo varchar(100) not null,
	descripcion text not null,
	id_estado tinyint(1) unsigned not null,
	fecha_alta datetime not null,
	fecha_modificacion datetime not null,
	fecha_baja datetime,
	foreign key(id_estado)
	references estados(id_estado)
	on delete cascade
	on update cascade
)ENGINE=innoDB;

INSERT INTO estados(id_estado, nombre)
VALUES
(1, 'Pendiente'),
(2, 'En proceso'),
(3, 'Finalizada'),
(4, 'Cancelada');

-- Dumping structure for table vue.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table vue.items: ~0 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `code`, `name`, `created_at`, `updated_at`) VALUES
	(1, '1', 'Boominathan', NULL, NULL);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
