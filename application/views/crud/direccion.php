<!DOCTYPE html>
<html>
<head>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.js"></script>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title></title>
</head>
<body>
  <form action="" @submit.prevent="enviarFormulario">
    <input type="text" 
		   placeholder="Nombre" 
		   v-model="nombre">
	  
    <input type="text" 
		   placeholder="Ciudad" 
		   v-model="ciudad">
	  
    <p>
		¿Añadir dirección?
		<input type="checkbox" 
			   v-model="anadirDireccion"> 
	</p>
	  
	<textarea v-show="anadirDireccion" name="" id="" cols="30" rows="10"
			  v-model="direccion">
	</textarea>
	  
	<input type="submit" value="Enviar">
  </form>
	
	<pre>{{ $data | json }}</pre>
</body>
<script>
    new Vue({
	el: 'body',
	
	data: {
		nombre: '',
		ciudad: '',
		anadirDireccion: false,
		direccion: '',
	},
	
	methods: {
		enviarFormulario: function() {
			console.log(this.$data);
		}
	}
});
</script>
</html>