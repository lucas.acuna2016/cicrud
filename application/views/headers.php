<!DOCTYPE html>
<html lang='es'>
<head>
	<meta charset="utf-8" />
	<title> Lista de tareas </title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/bootstrap-responsive.min.css" />
	<script type="text/javascript" src="<?php echo base_url() ?>js/vue-resource.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>js/app.js"></script>

</head>