<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formulario extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view('crud/agregar');
    }
    public function direccion()
    {
        $this->load->view('crud/direccion');
    }

}