<?php defined('BASEPATH') OR exit('No direct script access allowed');

class menu extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->library('menu',array('Inicio','contacto','Ingresar'));
		$data['menu_init'] = $this->menu->construirMenu();
		$this->load->view('headers');
		$this->load->view('tareas/menu', $data);
	}


}